# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:15:59 2020

@author: jackb
"""

from PIL import Image, ImageDraw

def alternateRings(im1, im2, numCircles = 10):
    im2 = im2.resize (im1.size)
    mask = Image.new("L", im1.size, 0)
    # find center of image
    center = (im1.size[0]//2, im1.size[1]//2)
    # calculate radius of rings based on numrings and size of mask
    radius = center[0]//numCircles

    #get drawing context
    draw = ImageDraw.Draw(mask)
    
    # draw rings
    for i in range(numCircles, 1, -1):
        #Using ellipse will generate distroted bars with heights proportional to distance from center
        draw.ellipse((center[0] - i * radius, center[1] - i * radius, center[0] + i * radius, center[1] + i * radius), fill = (0 if i % 2 == 0 else 255))    
    # compisite images and return
    out = Image.composite(im2, im1, mask)         
    return out