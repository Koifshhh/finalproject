# Author: Sabrina Honigman

# Provides the user with the ability to change the aspect ratio of an image

from PIL import Image

# Method that takes an image and the desired aspect ratio
# Uses the aspect ratio to determine how many pixels
# tall the image should be, does not change the width
# Uses the resize method from the Image module of Pillow
# to actual manipulate the size of the image
def resizeRatio(image, widthCoef, heightCoef):
    width = image.size[0]
    height = (width // widthCoef) * heightCoef

    return image.resize((width, height))

# Notes:
# I started this part of the project with the idea that
# the resize method would not accomplish what I wanted.
# This led me to try using an approach that involved making
# a numpy array of the new size and putting the original image
# into that array. This worked for most of the standard
# aspect ratios, but only when the new image was taller than the
# original. It did not work when the desired height was larger than
# the desired width.

# At this point I tried out the resize method because I could not
# remember why I thought it would not work. It ended up being exactly
# what I needed.
