#shuffle.py by Ben Soderberg
from PIL import Image
import random

def stitchHorizontal(image1, image2):
    """
    Takes two images and stitches them together horizontally.
    :param image1: The image stitched on the left side
    :param image2: The image stitched on the right side
    :return: The stitched image
    """
    (width1, height1) = image1.size
    (width2, height2) = image2.size

    resultWidth = width1 + width2
    resultHeight = max(height1, height2)

    stiched = Image.new("RGB", (resultWidth, resultHeight))
    stiched.paste(im = image1, box = (0,0))
    stiched.paste(im = image2, box = (width1, 0))
    return stiched

def stitchVertical(image1, image2):
    """
    Takes two images and stitches them together vertically.
    :param image1: The image stitched on the top
    :param image2: The image stitched on the bottom
    :return: The stitched image
    """
    (width1, height1) = image1.size
    (width2, height2) = image2.size

    resultWidth = max(width1, width2)
    resultHeight = height1 + height2

    stiched = Image.new("RGB", (resultWidth, resultHeight))
    stiched.paste(im = image1, box = (0,0))
    stiched.paste(im = image2, box = (0, height1))
    return stiched

def shuffle2x2(image):
    """
    Splits the given image into a two by two grid and returns a new image
    composed of the split tile images shuffled together.
    :param image: The image to be shuffled
    :return: A shuffled image
    """
    (width, height) = image.size
    # Using crop() to split the image into the tiles
    topLeft = image.crop((0, 0, width/2, height/2))
    topRight = image.crop((width/2, 0, width, height/2))
    botLeft = image.crop((0, height/2, width/2, height))
    botRight = image.crop((width/2, height/2, width, height))
    result = Image.new("RGB", (width, height))
    tiles = [topLeft, topRight, botLeft, botRight]
    # Randomly shuffling the tiles together
    random.shuffle(tiles)
    topHalf = stitchHorizontal(tiles[0], tiles[1])
    botHalf = stitchHorizontal(tiles[2], tiles[3])
    return stitchVertical(topHalf, botHalf)

def shuffle3x3(image):
    (width, height) = image.size
    # Using crop() to split the image into the tiles
    topLeft = image.crop((0, 0, width/3, height/3))
    topCenter = image.crop((width/3, 0, 2*width/3, height/3))
    topRight = image.crop((2*width/3, 0, width, height/3))
    midLeft = image.crop((0, height/3, width/3, 2*height/3))
    midCenter = image.crop((width/3, height/3, 2*width/3, 2*height/3))
    midRight = image.crop((2*width/3, height/3, width, 2*height/3))
    botLeft = image.crop((0, 2*height/3, width/3, height))
    botCenter = image.crop((width/3, 2*height/3, 2*width/3, height))
    botRight = image.crop((2*width/3, 2*height/3, width, height))
    result = Image.new("RGB", (width, height))
    tiles = [topLeft, topCenter, topRight, midLeft, midCenter, midRight, botLeft, botCenter, botRight]
    # Randomly shuffling the tiles together
    random.shuffle(tiles)
    # Assembling the parts of the final image
    top = stitchHorizontal(tiles[0], stitchHorizontal(tiles[1], tiles[2]))
    mid = stitchHorizontal(tiles[3], stitchHorizontal(tiles[4], tiles[5]))
    bot = stitchHorizontal(tiles[6], stitchHorizontal(tiles[7], tiles[8]))
    return stitchVertical(top, stitchVertical(mid, bot))